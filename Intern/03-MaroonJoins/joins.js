use School


db.Students.insertMany([
    {
    Sid: 1,
    name: "name1",
    age: 20,
    dept: "eie",
    ExamId: 1
    },
    {
    Sid: 2,
    name: "name2",
    age: 20,
    dept: "it",
    ExamId: 2
    },
    {
    Sid: 3,
    name: "name3",
    age: 21,
    dept: "cse",
    },
    {
    Sid: 4,
    name: "name4",
    age: 19,
    dept: "eie",
    ExamId: 1
    }]
)

db.Exams.insertMany([
    {
        Eid: 1,
        name: "python",
        dept: "it"
    },
    {
        Eid: 2,
        name: "java",
        dept: "it"
    },
    {
        Eid: 3,
        name: "MC",
        dept: "eie"
    },
    {
        Eid: 4,
        name: "OS",
        dept: "cse"
    }]
)

db.Students.aggregate([
    {
       $lookup: {
          from: "Exams",
          localField: "ExamId",
          foreignField: "Eid",
          as: "Exam"
       }
    },
    { $unwind: "$Exam"},
    { $set: {subject: "$Exam.name"}},
    { $set: {DeptSub: "$Exam.dept"} },
    { $project: {Exam: 0, _id: 0, ExamId:0}},
    // To Store In Database
    {$out: "StudentExams"}
 ])



db.Students.find()

db.StudentExams.find()
