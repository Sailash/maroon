import pymongo
from pymongo import MongoClient

class CsvRead:

    def __init__(self):
        # self.cluster = MongoClient("127.0.0.1", 27017)
        self.cluster = MongoClient("mongodb+srv://dbuser:MzboKH6LuanElRRr@cluster0.ofca3.mongodb.net/myFirstDatabase?retryWrites=true&w=majorit")
        self.TestDB = self.cluster["TestDB"]
        self.TestData = self.TestDB["TestData1"]        
        self.collection = []

    def readCsv(self):
        try:
            with open("user.csv", "r") as csvData:
                header = csvData.readline().strip("\n")
                header = [i for i in header.split(";")]
                print(header)
                while True:
                    data = csvData.readline().strip("\n")
                    if data == "": break
                    s = data.split(";")
                    document = {i:j for i,j in zip(header,s)}
                    self.collection.append(document)
        except Exception as e:
            print(e)

    def insertMongo(self):
        try:
            self.readCsv()
            self.TestData.insert_many(self.collection)
        except:
            print("Insert Exception")

    def readMongo(self):
        try:
            for i in self.TestData.find():
                print(i)
        except Exception as e:
            print(e)
    
    def deleteMongo(self):
        try:
            self.TestData.delete_many({})
            print("Deleted From Database")
        except Exception as e:
            print(e)


c = CsvRead()
c.insertMongo()
c.readMongo()
# c.deleteMongo()
