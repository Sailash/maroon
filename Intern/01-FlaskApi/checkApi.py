import requests

class Api:
  def __init__(self, ip, name):
    self.URL = f"http://{ip}:8080/{name}"

  def get(self):
    r = requests.get(self.URL)
    return r.text

  def post(self, command):
    r = requests.post(self.URL, command)
    return r.text

  def delete(self):
    r = requests.delete(self.URL)
    return r.text

  def put(self, command):
    r = requests.put(self.URL, command)

    return r.text



ip = "127.0.0.1"
name = "sail"
sail = Api(ip, name)

resp = sail.get()
print(resp)

# resp = sail.post({
#   "address": "chennai", 
#   "age": 19.0, 
#   "dob": "11-12-2000", 
#   "exam": "c++", 
#   "mark": 95.0, 
#   "staff": [
#     "name 20", 
#     "name 10"
#   ], 
#   "work": "Maroon"
# })
# print(resp)


# resp = sail.delete()
# print(resp)

# resp = sail.put({
#   "address": "py", 
#   "age": 19.0, 
#   "dob": "11-12-2000", 
#   "exam": "c++", 
#   "mark": 95.0, 
#   "staff": [
#     "name 20", 
#     "name 10"
#   ], 
#   "work": "Maroon"
# })
# print(resp)
