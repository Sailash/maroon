from flask import Flask, render_template, request, redirect, jsonify
from flask_restful import Api, Resource
from pymongo import MongoClient
from werkzeug.datastructures import ImmutableMultiDict

app = Flask(__name__)
api = Api(app)

cluster = MongoClient("127.0.0.1", 27017)
# cluster = MongoClient("mongodb+srv://dbuser:MzboKH6LuanElRRr@cluster0.ofca3.mongodb.net/myFirstDatabase?retryWrites=true&w=majorit")
TestDB = cluster["TestDB"]
TestData = TestDB["TestData"]

class RESTapi(Resource):
    def get(self, name):
        try:
            mongoFetch = list(TestData.find({"name" : name}, {"_id": 0}))
            return jsonify(*mongoFetch)
        except:
            pass

    def post(self,name):
        try:
            data = request.form.to_dict(flat=False)
            data = { i: data[i][0] if len(data[i]) <= 1 else data[i] for i in data}
            data["name"] = name
            # print(data)
            TestData.insert_one(data)
            return {"inserted": "ok"}
        except:
            pass

    def delete(self, name):
        try:
            TestData.delete_one({"name": name})
            return {"deleted": "ok"}
        except:
            pass

    def put(self, name):
        try:
            self.delete(name)
            self.post(name)
            return {"updated": "ok"}
        except:
            pass


api.add_resource(RESTapi, "/<name>/")

def readName():
    mongoFetch = []
    for i in TestData.find({},{"name": 1, "_id": 0}):
        mongoFetch.append(i["name"])
    return (mongoFetch)

@app.route("/", methods=["POST", "GET"])
def root():
    return render_template("index.html", name=readName())

@app.route("/<path:path>/")
def exception(path):
    return {}

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
